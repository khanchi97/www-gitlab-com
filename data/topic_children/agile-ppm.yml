title: GitLab's comprehensive guide to Agile project and portfolio management (PPM)
seo_title: GitLab's guide to Agile project and portfolio management (PPM)
description: Agile project and portfolio management (PPM)
header_body: Everything you need to know about Agile project and portfolio management (PPM)
canonical_path: "'topics/agile-delivery/agileppm'"
file_name: /agile-ppm/
parent_topic: agile-delivery
twitter_image: /images/opengraph/auto-devops.svg
related_content:
  - url: https://about.gitlab.com/blog/2021/05/19/agile-planning-with-a-devops-platform/
    title: Agile planning with a DevOps platform
body: >-
  If there’s an Agile secret weapon hiding in plain sight, it’s project and
  portfolio management, or what some call PPM. Master Agile PPM and not only
  will your teams get better software out the door faster, your entire
  organization’s cross-functional collaboration will improve dramatically. Agile
  is not just for software developers: product managers, finance folks, the
  legal team and even the C-suite can benefit from Agile project management,
  particularly when it’s available as part of an all-in-one DevOps platform like
  GitLab.


  Here’s everything you need to know about Agile project and portfolio management:


  ## Where Agile, DevOps and GitLab meet


  To understand how to get the most out of Agile project and portfolio management it’s helpful to take a deep dive into how [Agile works on a DevOps platform](/blog/2018/03/05/gitlab-for-agile-software-development/).


  Breaking it down further, here are the Agile steps most teams follow, and how they work seamlessly with a DevOps platform:


  * Issues: Start with an issue that captures a single feature that delivers business value for users.

  * Tasks: Often, a user story is further separated into individual tasks. You can create a task list within an issue's description in GitLab, to further identify those individual tasks.

  * Issue boards: Everything is in one place. Track issues and communicate progress without switching between products. One interface to follow your issues from backlog to done.

  * Epics: Manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones with epics.

  * Milestones: Track issues and merge requests created to achieve a broader goal in a certain period of time with GitLab milestones.

  * Roadmaps: Start date and/or due date can be visualized in a form of a timeline. The epics roadmap page shows such a visualization for all the epics which are under a group and/or its subgroups.

  * Labels: Create and assigned to individual issues, which then allows you to filter the issue lists by a single label or multiple labels.

  * Burndown Chart: Track work in real time, and mitigate risks as they arise. Burndown charts allow teams to visualize the work scoped in a current sprint as they are being completed.

  * Points and Estimation: Indicate the estimated effort with issues by assigning weight attributes and indicate estimated effort.

  * Collaboration: The ability to contribute conversationally is offered throughout GitLab in issues, epics, merge requests, commits, and more!

  * Traceability: Align your team's issues with subsequent merge requests that give you complete traceability from issue creation to end once the related pipeline passes.

  * Wikis: A system for documentation called Wiki, if you are wanting to keep your documentation in the same project where your code resides.

  * Enterprise Agile Frameworks: Large enterprises have adopted Agile at enterprise scale using a variety of frameworks. GitLab can support SAFe, Spotify, Disciplined Agile Delivery and more.


  ## Manage any project


  It’s easy to forget that every part of an organization needs help with planning and project management, not just those involved with software development. We’re always happy to “dogfood” our own tool: here’s how we [use GitLab for marketing project management](/blog/2019/12/06/gitlab-for-project-management-one/) and how one team [manages partner alliances](/blog/2021/05/11/project-management-using-gitlab-platform/). In our experience, [Agile planning works best](/blog/2021/05/19/agile-planning-with-a-devops-platform/) with a DevOps platform.


  ## Fine tune the process


  Since Agile project and portfolio management has a lot of moving parts, we created a quick [hands-on demo](https://www.youtube.com/watch?v=VR2r1TJCDew) and a [more in-depth option.](https://www.youtube.com/watch?v=YzlI2z_bGAo)


  If you’re trying to project manage multiple Agile teams, [watch a walk-through](https://www.youtube.com/watch?v=VR2r1TJCDew) of how to do that. And if you’re confused about how GitLab’s issues work, [watch this](https://www.youtube.com/watch?v=CiolDtBIOA0).


  Wonder how it all can work using the Scaled Agile Framework? [Here’s everything you need to know](https://www.youtube.com/watch?v=PmFFlTH2DQk).


  ## Agile project management in the real world


  The British Geological Society needed a way its scientific staff could stay involved with the software development team, and the solution was GitLab’s DevOps platform and [it’s project management capabilities](https://about.gitlab.com/customers/bgs/).
pull_quote:
  - quote: It’s easy to forget that every part of an organization needs help with
      planning and project management, not just those involved with software
      development.
