---
layout: handbook-page-toc
title: "Community Programs Applications Automated Workflows"
description: "This page describes the automated workflows for the GitLab for Education, GitLab for Open Source, and GitLab for Startups applications."  
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


During 2021, we are working on automation projects for the Community Programs Applications Workflow. This page is a work in progress as we document the workflow and troubleshoot the process.
{: .alert .alert-gitlab-orange}


## Overview

This page describes the automated workflows for the following community programs:

| Community Program  | Handbook Page | Website | Program Owner |
| ------ | ------ |------ | ----- |
| GitLab for Education | [EDU Handbook](/handbook/marketing/community-relations/education-program/) | [EDU website](/solutions/education/) | Christina Hupy |
| GitLab for Open Source | [OSS Handbook](/handbook/marketing/community-relations/opensource-program/) | [OSS Website](/solutions/open-source/) | Nuritzi Sanchez |
| GitLab for Startups | [Startups Handbook](/handbook/marketing/community-relations/startups-program/) | [GitLab for Startups](/solutions/startups/) | Nuritzi Sanchez |

The goals of this page are to:
 * Provide an overview of the automated community programs applications workflows
 * Store details for each step of the automated workflow
 * Allow transparency into our workflows to help others integrate with it

## Automated Application workflow

| Phase | Description |
| ------ | ------ |
| 0. [Application](/handbook/marketing/community-relations/community-operations/automated-community-programs/#phase-0-application) | SheerId hosted application form is filled out for OSS and EDU. YC applicants fill out Marketo Form.|
| 1. [Verification](/handbook/marketing/community-relations/community-operations/automated-community-program/#phase-1-verification) | Automated verification by SheerID for EDU. Document review for OSS. Manual verification for YC.|
| 2. [Booking](/handbook/marketing/community-relations/community-operations/community-program-applications/#phase-2-booking) | Success email directs applicant to a separate Community Programs Self-Checkout page for each program on the GitLab Customer Portal. Coupon code is entered during checkout. |
| 3. [Provisioning](/handbook/marketing/community-relations/community-operations/automated-community-programs/#phase-3-provisioning-and-phase-4-compliance) | Licenses are provisioned through the web direct process on the GitLab Customer Portal.|
| 4. [Compliance](/handbook/marketing/community-relations/community-operations/automated-community-programs/#phase-3-provisioning-and-phase-4-compliance) | Handled by Sales-Support and Billing Ops. |
| 5. [Renewal](/handbook/marketing/community-relations/community-operations/automated-community-programs/#phase-5-renewal) | Program members will repeat the process within 3-months of renewal.|
| 6. [Support](/handbook/marketing/community-relations/community-operations/automated-community-programs/#phase-5-renewal) | Each step of the automated application workflow has different set of potential errors and support flow.|

## PHASE 0: Application

GitLab uses a third party vendor, [SheerID](https://www.sheerid.com/shoppers/aboutsheerid/), for verification services. SheerId builds and hosts the application forms for the GitLab for Education Program and the GitLab for Open Source Program. All communications (email and browser notifications) sent during the verification phase for these two programs are hosted and sent by SheerID.

The DRI for SheerID is Christina Hupy. The contract is renewed on an annual basis and is based on the total number of verifications across the two programs.

[MySheerID](https://my.sheerid.com/) is the customer portal for the verification system. The portal contains details on each application form, a reporting system, and account settings. Anyone needing to access the MySheerId portal can request an account through an [access request](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/).

SheerID Customer Service specific to GitLab can be found [here](https://drive.google.com/file/d/13AW0BYt4HNFzGfM6iRuMtpjZ1J8tAXyI/view?usp=sharing).

## PHASE 1: Verification

The verification process differs by program. The verification process for each program can be found on the pages below:

- [GitLab for Education verification](/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/edu-program-verification/)
- [GitLab for Open Source verification](/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/oss-program-verification/)
- [GitLab for Startups verification](/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/startups-program-verification/)

### Document Review
Once an applicant has submitted the requested fields on the form, the application processing team will be able to find all of the application information in [Document Review](https://my.sheerid.com/authentication/) on the MySheerID customer portal. 

Only people who have been added to the MySheerID customer portal have access to Document Review.


Below are instructions for how the application processing team can review applications for the GitLab for Open Source program or escalated cases for the GitLab for Education program.

**How to begin reviewing:**

1. Login to [MySheerID](https://my.sheerid.com/authentication/)
2. Navigate to `Document` review
3. Select `Begin` in the upper left hand corner (Make sure to adjust the filter to chronological order to make sure to address the users who have been waiting the longest).

#### Determining qualification  

 1. Do the "Documents" appear legitimate? 
    - For OSS: Did the applicant submit 3 screenshots of their GitLab project? Please [see our docs]() FIXME for details on what the screenshots should show.
    - For EDU: FIXME

 1. Does the affiliation type on the document match a qualified open source project? (FIXME: Need to figure out if this is needed for OSS)

 1. Make sure you have selected an affiliation type otherwise you will not be able to approve the document. (FIXME: Need to figure out if this is needed for OSS)

 1. If all of the above are true select `Check All`.

 1. Submit! 

 FIXME: Add information on what to do if it needs to be rejected. Do the above instructions work for both qualifying and non-qualifying applications? 


#### Documents under question

If there is a discrepancy regarding the legitimacy of the document and or the application requires help from another team member, navigate to the comments section at the bottom of the page. 

 1. Select `Comment` and describe the issue as clear as possible
 1. Select `Escalate` to tag a team member and place the application in the escalation queue


### Coupon Codes

Upon successful verification, the applicant will receive an email with instructions to obtain their license and a unique coupon code.

The coupon codes are generated by the fulfillment team at GitLab via a coupon code generator. The DRI for the coupon code generator is the [fulfillment team](https://about.gitlab.com/direction/fulfillment/) and the documentation is [here](https://gitlab.com/gitlab-org/customers-gitlab-com/-/merge_requests/3082/diffs).

 Coupon codes are provided to SheerID by Christina Hupy via an email to helpdesk@sheerid.com. The coupon codes are provided in an individual .csv for each program. The .csv should only have one column with no header. Enough coupon codes to cover one year of verifications for each program will be provided at a time. The number of coupon codes per year is determined by estimated the number of applications expected per quarter + the number of expected renewals * an average re-verification factor.  


## PHASE 2: Booking

The success email will contain a direct link to a program specific Self-Checkout page for each of the three programs (EDU/OSS/YC) in the GitLab Customer Portal. The program specific Self-Checkout pages are not available directly in the GitLab Customer Portal without the direct link. The applicant will need to enter the unique coupon code during the Self-Checkout process. The handbook for the Custom Self-Checkout flow on the GitLab Customer Portal is here [TBD].

### Terms

During the Self-Checkout process, the applicant will need to accept terms specific to the relevant program: [GitLab for Education Program Agreement](https://about.gitlab.com/handbook/legal/education-agreement/) or  [GitLab for Open Source Agreement](https://about.gitlab.com/handbook/legal/opensource-agreement/). The GitLab for Startups Terms are presented directly upon checkout.  

The GitLab for Startups Terms:

If you meet the requirements of the Start-Up Program, you will be eligible for ONE (1) of the following Options:

Option 1: Receive twelve (12) months Ultimate [SaaS or Self-Managed] at no cost, without Support.
Option 2: Receive twelve (12) months Ultimate [SaaS or Self-Managed] with Support, for $59.40(USD) per User (per Year).
Both options are available for ONE (1) TIME use, and only for ONE (1) YEAR. Renewal of the User(s) will be at the current published List Price.

Your use of the GitLab Software is subject to the GitLab Subscription Agreement. For Option 1, the Software is provided as “Free Software”.

## PHASE 3: Provisioning

Licenses will be provisioned directly during Self-checkout process via the WebDirect flow.  


## PHASE 4: Compliance

Compliance is handled by Sales-Support and Billing Ops. This phase results in granting the license and notifying the customer of how to access the licenses.

## PHASE 5: Renewal

The [renewal phase](https://gitlab.com/groups/gitlab-org/-/epics/5711) of the Community Efficiency Project is currently in the scoping stage and work is expected to be completed in the 14.1 milestone for the fulfillment team beginning 2021-07-21. This work will add the ability to renew license upon successful verification directly in the GitLab Customers Portal via the program specific Self-Checkout page.
{: .alert .alert-warning}

Until the renewal phase is completed, the renewal process will remain as stated on the [Community Programs Applications Workflow](https://about.gitlab.com/handbook/marketing/community-relations/community-operations/community-program-applications/#phase-5-renewal) page.


## PHASE 6: Support

Each step of the automated application workflow has different set of potential errors and support flow.


|Phase|Source|Error|DRI|Action|
|----|----|----|---|---|--------|
| Verification| SheerID Application| False Rejection EDU | SheerID  | Contact SheerID from Rejection Email.  |
| Verification| SheerID Application| False Rejection OSS | @nuritzi / OSS Program | Contact opensource@gitlab.com from rejection email |
| Verification| SheerID Application | Never received success email. |SheerId | [SheerId Help Center FAQ](https://offers.sheerid.com/sheerid/help-center/?name=no-email) - Form resends email|
| Verification| SheerID Application | Deletes success email. | SheerId | [SheerId Help Center FAQ](https://offers.sheerid.com/sheerid/help-center/?name=no-email) - Form resends email|
| Verification| SheerID Application | Form not responding or something goes wrong with form. | SheerID | [Contact SheerID Support Team ](https://offers.sheerid.com/sheerid/help-center/?name=form-doesnt-work)|
| Verification| SheerID Application | Applicant makes a case to SheerID that EDU rejection was in error but SheerID cannot resolve. | @c_hupy | SheerID emails education@gitlab.com with details. GitLab EDU team resolves. |
|Booking| GitLab Customer Portal | Coupon Code has already been used. | GitLab Support |Error message on the portal. `The code has already been used.` There is no CTA on the portal, user will go back to email which directs them to open a support ticket under [`Issues with billing, purchasing, subscriptions, or licenses.`](https://about.gitlab.com/support/#issues-with-billing-purchasing-subscriptions-or-licenses)|  
|Booking| GitLab Customer Portal | Coupon Code is invalid. | GitLab Support | Error message on the portal. `This code is not valid. Try re-entering the code from your email`. There is no CTA message on the portal, the user will go back to email which directs them to open a support ticket under [`Issues with billing, purchasing, subscriptions, or licenses.`](https://about.gitlab.com/support/#issues-with-billing-purchasing-subscriptions-or-licenses)|
|Fulfillment| GitLab Customer Portal | Any problems with Customer Portal after coupon code succeeds.| GitLab Support | Open Support Ticket [`Issues with billing, purchasing, subscriptions, or licenses.`](https://about.gitlab.com/support/#issues-with-billing-purchasing-subscriptions-or-licenses) )|
