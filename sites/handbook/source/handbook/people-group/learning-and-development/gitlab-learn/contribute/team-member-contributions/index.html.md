---
layout: handbook-page-toc
title: Become a GitLab Learning Evangelist
description: "Contribution process for GitLab team members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contributing to GitLab Learn as a Team Member

Team members have two paths they can follow to contribute learning content to the GitLab Learn platform. Review these scenarioes to decide which role best fits your needs:

| Scenario | Contributor Role |
| ----- | ----- |
| I want to make suggestions to team members on what existing learning material might be best for their professional development journey. For example, I want to pull together some LinkedIn Learning courses that would help them develop new skills | Curator |
| I'd like you create a brand new learning pathway for team members or the wider community. I plan on creating my own content, recording videos, designing handbook pages, and more to deliver this course | Learning Evangelist |

## Become a GitLab Learn Curator

Curators in GitLab Learn can organize existing learning content in their specific team Groups or Channels to broadcast, organize, and encourage professional development. This role is perfect for PBPs, leaders, or managers who'd like to make suggestions to their team regarding what LinkedIn Learning courses might be most beneficial!

## GitLab Learn Curator Pathway

### Part 1: Curating content to a channel

1. Watch the training on [how to share and curate content in GitLab Learn to a channel](https://youtu.be/8B7QJpd2Fp4)

### Part 2: Customizing channels

1. Watch the training on [how to customize channels with custom carousels](https://youtu.be/R0rLl6NB44g)

### Part 3: Setting up your channel

You're ready to build your channel! Please follow the steps on the [Work with Us](https://about.gitlab.com/handbook/people-group/learning-and-development/work-with-us/#creating-a-gitlab-learn-channel) handbook page to open an issue for your new channel.

If your team's channel already exists, please skip to Part 4

### Part 4: Add the `Curator` role to your EdCast profile

Follow the steps outlined in the handbook to open an [individual access request for the `Curator` role in EdCast](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/). First assign this AR to your manager for approval and to alert your manager that you've taken the relevant pathway and will be curating content for your team. After approval from your manager, please assign the AR to `@slee24`.

Once processed, you're ready to start curating content on your team's channel! Please reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) if you need additional support.

## Become a GitLab Learning Evangelist

The Learning and Development team has created an opportunity for GitLab Team Members to become learning content contributors, called Learning Evangelists, in the GitLab Learn platform.

We've created a learning path to become a GitLab Learning Evangelist to

1. Ensure that all content contributors are trained on the EdCast platform
1. Communicate how handbook first is applied to e-learning content 
1. Ensure that content across the platform is cohesive and avoids repetition
1. Democratize the learning process and provide space for all voices to be heard
1. Keep content and look and feel of the platform aligned with overall brand

**Why become a Learning Evangelist?**

Learning Evangelists in GitLab Learn have the opportunity to:

1. Be the face of learning for your team and organization
1. Dedicate your passion for learning and developing others by contributing content to the LXP
1. Influence team members to take time out to learn new skills, build courses for the community, and feature training material for all to consume!


## GitLab Learning Evangelist Learning Pathway

Upon completion of this learning path, GitLab Team members will earn the Learning Evangelist badge and be granted platform permissions that will allow them to contribute learning content to the GitLab platform.

### Part 1 - Understanding GitLab Learn

1. Read the [GitLab Learn handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/#gitlab-learn) to learn about the creation and purpose behind GitLab Learn, and EdCast Learning Experience Platform. 
1. Watch the [EdCast User Experience Overview Video](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/admin/#best-practices-for-content-creation-and-organization)
1. Knowledge Assessment

### Part 2 - Handbook first learning content

1. Read [interactive learning handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/interactive-learning/) to understand how handbook first learning content is created for GitLab Learn.
1. Review an [example](/handbook/people-group/learning-and-development/interactive-learning/#handbook-first-course-example) of handbook first learning content in GitLab Learn using an Articulate 360 course
1. Watch this [interview with Sid](/handbook/people-group/learning-and-development/#handbook-first-training-content) discussing the organization of handbook first learning content in the GitLab handbook
1. Knowledge Assessment


### Part 3 - Creating content in EdCast

1. Get familiar with [how to navigate admin docs for GitLab Learn](/handbook/people-group/learning-and-development/gitlab-learn/admin/#navigating-gitlab-learn-admin-content)
1. Watch training videos for uploading content in GitLab Learn
     - [Example: Contribute an interesting article or video as a SmartCard](/handbook/people-group/learning-and-development/gitlab-learn/admin/#contribute-an-interesting-article-or-video)
     - [Example: Contribute a new Pathway or Journey](/handbook/people-group/learning-and-development/gitlab-learn/admin/#contribute-a-new-pathway-or-journey)
     - [Order of Operations for Public and Privaet Content](/handbook/people-group/learning-and-development/gitlab-learn/admin/#order-of-operations-for-content-creation)
     - [Building Public and Private SmartCards Example](/handbook/people-group/learning-and-development/gitlab-learn/admin/#step-2-build-smartcards-depending-on-content-status)
1. Review the process and watch the example video for choosing a [badge](/handbook/people-group/learning-and-development/gitlab-learn/admin/#badges) for your course
1. Watch the [video](/handbook/people-group/learning-and-development/gitlab-learn/admin/#pathways) explaining how to use lock and leap functions in Pathways and Journeys
1. Knowledge Assessment

### Part 4 - Keeping content aligned with look and feel

1. Review the [image guidelines](/handbook/people-group/learning-and-development/gitlab-learn/admin/#image-guidelines) for adding images to GitLab Learn
1. Explore existing photos in the [GitLab Learn photo drive](https://drive.google.com/drive/folders/1GvE-MUtHzGbZ9KX-16bsTvwFDn-Cd4hy?ths=true) Bookmark this page as a resource for future content creation. 
1. Knowledge Assessment

### Part 5 - Peer review with L&D team or other Learning Evangelist

1. Review the process for completeing the [peer editing process](/handbook/people-group/learning-and-development/gitlab-learn/admin/#peer-review-content-before-publishing) before sharing content.
1. Review strategies for [Sharing content with learners](/handbook/people-group/learning-and-development/gitlab-learn/admin/#sharing-content-with-learners)
1. Knowledge Assessment

## Add the `Learning Evangelist` role to your EdCast profile

Follow the steps outlined in the handbook to open an [individual access request for the `Learning Evangelist` role in EdCast](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/). First assign this AR to your manager for approval and to alert your manager that you've taken the relevant pathway and will be creating and contributing learning content for your team. After approval from your manager, please assign the AR to `@slee24`. and assign the AR to `@slee24`.

Once processed, you're ready to start contributing content to GitLab Learn! Please reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) if you need additional support.


## After earning the Learning Evangelist Badge

After you've passed the final learning assessment and earned the GitLab Learning Evangelist badge, you're ready to start contributing to GitLab Learn!

The Learning and Development team will monitor your progress on the learning path and update your platform permissions when you've earned the badge. If you've completed the learning path but have not had your permissions updated yet, please reach out to the L&D team in Slack for support.

## FAQ from Learning Evangelists

1. Is there a required amount of content I need to contribute?
1. Can I access Articulate 360 or other authoring tools?
1. Where should I go if I have questions or need support with the GitLab Learn platform?
